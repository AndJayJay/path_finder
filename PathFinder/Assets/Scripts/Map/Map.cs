﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;

/// <summary>
/// Representation of map
/// </summary>
[System.Serializable]
public class Map
{
    #region Fields

    /// <summary>
    /// Maximal size of obstacles
    /// </summary>
    int maxObstacleSize = 2;

    #endregion

    #region Properties

    [System.NonSerialized]
    ListVector2IntEvent _onObstaclesChange = null;
    /// <summary>
    /// Property for event of changing obstacles on map
    /// </summary>
    /// <value>Property gets/sets the field with event</value>
    public ListVector2IntEvent OnObstaclesChange
    {
        get
        {
            //event jest definiowany w get'cie aby zapobiec problemom po deserializacji
            if (_onObstaclesChange == null)
                _onObstaclesChange = new ListVector2IntEvent();
            return _onObstaclesChange;
        }
    }

    [System.NonSerialized]
    ListVector2IntEvent _onStartEndChange = null;
    /// <summary>
    /// Property for event of changing start/end points on map
    /// </summary>
    /// <value>Property gets/sets the field with event</value>
    public ListVector2IntEvent OnStartEndChange
    {
        get
        {
            //event jest definiowany w get'cie aby zapobiec problemom po deserializacji
            if (_onStartEndChange == null)
                _onStartEndChange = new ListVector2IntEvent();
            return _onStartEndChange;
        }
    }
    [System.NonSerialized]
    ListVector2IntEvent _onShortestPathChange = null;
    /// <summary>
    /// Property for event of changing shortest path on map
    /// </summary>
    /// <value>Property gets/sets the field with event</value>
    public ListVector2IntEvent OnShortestPathChange
    {
        get
        {
            //event jest definiowany w get'cie aby zapobiec problemom po deserializacji
            if (_onShortestPathChange == null)
                _onShortestPathChange = new ListVector2IntEvent();
            return _onShortestPathChange;
        }
    }

    List<Vector2Int> _shortestPath = new List<Vector2Int>();
    /// <summary>
    /// Property for list of shortest path points from start to end point on map
    /// </summary>
    /// <value>Property gets/sets the field with shortest path points. Set invoke event OnShortestPathChange</value>
    public List<Vector2Int> ShortestPath
    {
        get
        {
            return _shortestPath;
        }
        set
        {
            if (value != _shortestPath)
            {
                _shortestPath = value;
                OnShortestPathChange.Invoke(_shortestPath);
            }
        }
    }

    List<Obstacle> _obstacles = new List<Obstacle>();
    /// <summary>
    /// Property for list of obstacles on map
    /// </summary>
    /// <value>Property gets the field with list of obstacles on map</value>
    public List<Obstacle> Obstacles
    {
        get
        {
            return _obstacles;
        }
    }

    Vector2Int _startPoint = new Vector2Int(-1, -1);
    /// <summary>
    /// Property for start point on map
    /// </summary>
    /// <value>Property gets the start point on map</value>
    public Vector2Int StartPoint
    {
        get
        {
            return _startPoint;
        }
    }

    Vector2Int _endPoint = new Vector2Int(-1, -1);
    /// <summary>
    /// Property for end point on map
    /// </summary>
    /// <value>Property gets the end point on map</value>
    public Vector2Int EndPoint
    {
        get
        {
            return _endPoint;
        }
    }

    int _size;
    /// <summary>
    /// Property for size of map
    /// </summary>
    /// <value>Property gets the size of map</value>
    public int Size
    {
        get
        {
            return _size;
        }
    }

    #endregion

    #region Methods

    /// <summary>
    /// Constructor for creating new map
    /// </summary>
    /// <param name="size">Size of created map</param>
    public Map(int size)
    {
        this._size = size;
    }

    /// <summary>
    /// Generate new obstacles for map. Invoke event OnObstaclesChange
    /// </summary>
    /// <param name="numberObstacles">Number of new obstacles</param>
    public void GenerateObstacles(int numberObstacles)
    {
        Obstacles.Clear();
        ShortestPath.Clear();

        //Lista dostępnych pozycji dla przeszkód
        List<Vector2Int> avaibleObstaclesPositionsList = new List<Vector2Int>();
        //Lista pozycji zajmowanych przez przeszkody
        List<Vector2Int> allObstaclesPositionsList = new List<Vector2Int>();

        //Dodawanie do listy dostępnych miejsc wszystkich miejsc na mapie
        for (int i = 0; i < Size; i++)
            for (int j = 0; j < Size; j++)
                avaibleObstaclesPositionsList.Add(new Vector2Int(i, j));

        //Usunięcie z listy dostępnych miejsc punktu startu i końca, jeśli istnieją
        if (StartPoint != new Vector2Int(-1, -1) && EndPoint != new Vector2Int(-1, -1))
        {
            avaibleObstaclesPositionsList.Remove(StartPoint);
            avaibleObstaclesPositionsList.Remove(EndPoint);
        }

        //Dodawanie nowych przeszkód do list przeszkód
        Obstacle newObstacle;
        for (int i = 0; i < numberObstacles; i++)
        {
            //Tworzenie nowej przeszkody w losowym miejscu
            newObstacle = CreateObstacle(avaibleObstaclesPositionsList[Random.Range(0, avaibleObstaclesPositionsList.Count)]);

            var obstaclePositions = newObstacle.GetPositions();
            //Uzupełnianie listy pozycji zajmowanych przez przeszkody
            allObstaclesPositionsList.AddRange(obstaclePositions);
            //Aktualizacja listy dostępnych miejsc na mapie
            foreach (Vector2Int obstaclePoints in obstaclePositions)
            {
                avaibleObstaclesPositionsList.Remove(obstaclePoints);
            }

            //Zabezpieczenie aby zawsze istniało miejsce na utworzenie startu i końca
            if (avaibleObstaclesPositionsList.Count <= maxObstacleSize* maxObstacleSize)
                break;
        }

        OnObstaclesChange.Invoke(allObstaclesPositionsList);        
    }

    /// <summary>
    /// Create new obstacle
    /// </summary>
    /// <param name="obstaclePosition">Start position on new obstacle</param>
    /// <returns>Obstacle with selected start position</returns>
    Obstacle CreateObstacle(Vector2Int obstaclePosition)
    {
        Vector2Int obstacleSize;

        //Losowanie rozmiaru przeszkody
        obstacleSize = new Vector2Int(Random.Range(1, maxObstacleSize + 1), Random.Range(1, maxObstacleSize + 1));

        //Weryfikacja rozmiaru przeszkody pod względem rozmiaru mapy (Pojawienie się dużych przeszkód przy krawędzi mapy)
        obstacleSize.x = Mathf.Clamp(obstacleSize.x, 1, (Size - obstaclePosition.x));
        obstacleSize.y = Mathf.Clamp(obstacleSize.y, 1, (Size - obstaclePosition.y));
        Obstacle newObstacle = new Obstacle(obstaclePosition, obstacleSize);
        //Weryfikacja rozmiaru przeszkody pod względem punktu startu i stopu
        newObstacle.Shrink(StartPoint);
        newObstacle.Shrink(EndPoint);
        //Weryfikacja rozmiaru przeszkody pod względem już istniejących przeszkód
        foreach (Obstacle obstacle in Obstacles)
        {
            newObstacle.Shrink(obstacle.Position);
        }

        Obstacles.Add(newObstacle);
        return newObstacle;
    }

    /// <summary>
    /// Generate new start/end points for map. Invoke event OnStartEndChange
    /// </summary>
    public void GenerateStartEnd()
    {
        ShortestPath.Clear();

        //Lista zajętych miejsc przez przeszkody
        List<Vector2Int> occupiedPositionsList = GetAllObstaclesPositions();

        Vector2Int[] startEndPositions = new Vector2Int[2];

        //Losowanie miejsc dla punktu startu i końca
        for (int i = 0; i < 2; i++)
        {
            while (true)
            {
                Vector2Int tempPosition = new Vector2Int(Random.Range(0, _size), Random.Range(0, _size));

                //Weryfikacja czy wylosowany punkt jest dostępny
                if (!occupiedPositionsList.Contains(tempPosition))
                {
                    startEndPositions[i] = tempPosition;

                    occupiedPositionsList.Add(tempPosition);
                    break;
                }
            }
        }

        _startPoint = startEndPositions[0];
        _endPoint = startEndPositions[1];
        OnStartEndChange.Invoke(startEndPositions.ToList());
    }

    /// <summary>
    /// Create list of all positions occupied by obstacles on map
    /// </summary>
    /// <returns>List of occupied positions by obstacles</returns>
    public List<Vector2Int> GetAllObstaclesPositions()
    {
        List<Vector2Int> obstaclePositionList = new List<Vector2Int>();

        foreach (var obstacle in _obstacles)
        {
            obstaclePositionList.AddRange(obstacle.GetPositions());
        }

        return obstaclePositionList;
    }

    #endregion
}
