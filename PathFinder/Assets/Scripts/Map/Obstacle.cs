﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Representation of obstacle on map
/// </summary>
[System.Serializable]
public class Obstacle
{
    #region Properties
    Vector2Int _position;
    /// <summary>
    /// Property for start position of obstacle
    /// </summary>
    /// <value>Property gets the field with start position of obstacle</value>
    public Vector2Int Position
    {
        get
        {
            return _position;
        }
    }

    /// <summary>
    /// Property for size of obstacle
    /// </summary>
    /// <value>Property gets the field with size of obstacle</value>
    Vector2Int _size;
    public Vector2Int Size
    {
        get
        {
            return _size;
        }
    }
    #endregion

    #region Methods
    /// <summary>
    /// Constructor for creating obstacle with start position and size
    /// </summary>
    /// <param name="position">Start position of obstacle</param>
    /// <param name="size">Size of obstacle</param>
    public Obstacle(Vector2Int position, Vector2Int size)
    {
        _position = position;
        _size = size;
    }

    /// <summary>
    /// Generate list of positions occupied by obstacle
    /// </summary>
    /// <returns>List of occupied positions</returns>
    public List<Vector2Int> GetPositions()
    {
        List<Vector2Int> positionList = new List<Vector2Int>();

        for(int i=0;i<Size.x;i++)
            for(int j =0;j< Size.y;j++)
            {
                positionList.Add(new Vector2Int(i + Position.x, j + Position.y));
            }

        return positionList;
    }

    /// <summary>
    /// Shrink size of obstacle to avoid forbidden position
    /// </summary>
    /// <param name="forbiddenPosition">Forbidden position</param>
    public void Shrink(Vector2Int forbiddenPosition)
    {
        Vector2Int maxSize = forbiddenPosition - Position;

        if (maxSize.magnitude == 0)
            throw new System.Exception("Can't change obstacle size to 0,0");

        if (maxSize.x >= 0 && maxSize.x < Size.x && maxSize.y >= 0 && maxSize.y < Size.y)
        {
            if(maxSize.x>0)
                _size.x = Mathf.Clamp(Size.x, 1, maxSize.x);
            if (maxSize.y > 0)
                _size.y = Mathf.Clamp(Size.y, 1, maxSize.y);
        }

    }
    #endregion
}
