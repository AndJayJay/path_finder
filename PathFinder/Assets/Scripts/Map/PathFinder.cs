﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// Class for invoke algorithms of path finding in map
/// </summary>
public static class PathFinder
{
    #region Fields
    /// <summary>
    /// Enum type of avaible algorithms
    /// </summary>
    public enum Algorithm { BFS=0,AStar=1};

    /// <summary>
    /// Actually used map for calculation
    /// </summary>
    static Map map;

    /// <summary>
    /// Array of weights for each field in map
    /// </summary>
    static int[,] mapWeights;
    #endregion

    #region Methods

    /// <summary>
    /// Start selected path finding algorithm on map
    /// </summary>
    /// <param name="map">Map for analize</param>
    /// <param name="selectedAlgorithm">Selected algorithm type</param>
    /// <returns>Return true if path is founded</returns>
    public static bool FindPath(Map selectedMap, Algorithm selectedAlgorithm)
    {
        map = selectedMap;
        switch (selectedAlgorithm)
        {
            case Algorithm.BFS:
                return BFSAlgorithm();
            case Algorithm.AStar:
                return AStarAlgorithm();
            default:
                return false;
        }
    }

    /// <summary>
    /// Find positions of neighbour fields
    /// </summary>
    /// <returns>List of neighbour positions</returns>
    static List<Vector2Int> GetNeighbourFields(Vector2Int position)
    {
        List<Vector2Int> neighbourList = new List<Vector2Int>();

        if (position.x > 0)
            neighbourList.Add(position + new Vector2Int(-1, 0));

        if (position.y > 0)
            neighbourList.Add(position + new Vector2Int(0, -1));

        if (position.x < map.Size - 1)
            neighbourList.Add(position + new Vector2Int(1, 0));

        if (position.y < map.Size - 1)
            neighbourList.Add(position + new Vector2Int(0, 1));

        return neighbourList;
    }

    /// <summary>
    /// Breadth-first search algorithm
    /// </summary>
    /// <returns>Return true if path is founded</returns>
    static bool BFSAlgorithm()
    {
        mapWeights = new int[map.Size, map.Size];
        //Kolejka punktów do przeszukania
        Queue<Vector2Int> searchPoints = new Queue<Vector2Int>();
        //Flaga czy ścieżka została znaleziona
        bool pathIsFounded = false;
        //Aktualny dystans od punktu startu
        int stepLength = 1;

        //Uzupełnienie tablicy wag o położenie przeszkód
        foreach (Obstacle obstacle in map.Obstacles)
            foreach (Vector2Int obstaclePoint in obstacle.GetPositions())
                mapWeights[obstaclePoint.x, obstaclePoint.y] = -1;
        
        //Rozpoczęcie poszukiwań od punktu startu
        searchPoints.Enqueue(map.StartPoint);
        while (searchPoints.Count > 0)
        {
            int stepsNumber = searchPoints.Count;
            //Przeszukanie listy pól o tej samej odległości od startu
            for (int i = 0; i < stepsNumber; i++)
            {
                //Wybranie nowego punktu do sprawdzenia
                Vector2Int actualPoint = searchPoints.Dequeue();

                //Jeśli punkt jest punktem końcowym, to zakończ poszukiwania
                if (actualPoint.Equals(map.EndPoint))
                {
                    searchPoints.Clear();
                    map.ShortestPath = GetShortestPathForBFS(stepLength);
                    pathIsFounded = true;
                    break;
                }
                else
                {
                    //Nadaj wagę aktualnemu punktowi jako odległość od punktu startu
                    mapWeights[actualPoint.x, actualPoint.y] = stepLength;

                    //Sprawdź wszystkie pola sąsiadujące z aktualnym
                    foreach(Vector2Int neigbour in GetNeighbourFields(actualPoint))
                    {
                        //Jeśli waga dla tego pola nie została przypisana i pole nie zostało jeszcze przeszukane, to dodaj do poszukiwań
                        if(mapWeights[neigbour.x, neigbour.y] == 0 && !searchPoints.Any(x => x.Equals(neigbour)))
                            searchPoints.Enqueue(neigbour);
                    }
                }
            }
            stepLength++;
        }

        return pathIsFounded;
    }

    /// <summary>
    /// Create list of points for shortest path from mapWeights in BFS
    /// </summary>
    ///<param name="stepLength">Maximum distance to reach start point</param>
    /// <returns>List of shortest path points</returns>
    static List<Vector2Int> GetShortestPathForBFS(int stepLength)
    {
        //Rozpocznij wyznaczanie od punktu końca
        Vector2Int actualPoint = map.EndPoint;

        List<Vector2Int> path = new List<Vector2Int>();


        for (; stepLength > 2; stepLength--)
        {
            //Znajdź sąsiadów aktualnego pola
            foreach (Vector2Int neighbour in GetNeighbourFields(actualPoint))
            {
                //Jeśli sąsiad ma odległość o 1 mniejszą od początku niż aktualne pole to dodaj je do ścieżki
                if(mapWeights[neighbour.x, neighbour.y] == stepLength - 1)
                {
                    path.Insert(0, neighbour);
                    actualPoint = neighbour;
                    break;
                }

            }  
        }
        return path;        
    }

    /// <summary>
    /// A Star point class with values of weight functions
    /// </summary>
    class AStarPoint
    {
        #region Fields
        /// <summary>
        /// Position of point
        /// </summary>
        public Vector2Int position;
        /// <summary>
        /// Value of optymistic distance to get end point from start point
        /// </summary>
        public int f;
        /// <summary>
        /// Value of distance from start point
        /// </summary>
        public int g;
        /// <summary>
        /// Inform, if this point is verified before
        /// </summary>
        public bool visited = false;
        /// <summary>
        /// Previous AStar point
        /// </summary>
        public AStarPoint parent = null;
        #endregion

        #region Methods
        /// <summary>
        /// Constructor for create A Star point
        /// </summary>
        /// <param name="position">Position of point</param>
        /// <param name="fValue">Value of distance from start</param>
        /// <param name="gValue">Value of predicated distance to end</param>
        public AStarPoint(Vector2Int position,int fValue, int gValue)
        {
            this.position = position;
            f = fValue;
            g = gValue;
        }

        /// <summary>
        /// Constructor for create A Star point
        /// </summary>
        /// <param name="position">Position of point</param>
        public AStarPoint(Vector2Int position)
        {
            this.position = position;
        }
        #endregion
    }

    /// <summary>
    /// A Star Algorithm
    /// </summary>
    /// <returns>Returns true if path is founded</returns>
    static bool AStarAlgorithm()
    {
        //Komentarz: W algorytmie zastosowane zostały trzy oddzielne struktury do przechowywania informacji o stanie mapy
        //Każda z nich pełni oddzielne zadanie, które nie były możliwe do połączenia w jednej strukturze.
        //Tablica mapWeight zawiera informacje o położeniu przeszkód na mapie, lista searchAStarPoints pozwala na określenie
        //nieodwiedzonego pola o najmniejszej wartości funkcji odległości, natomiast tablica aStarPointsMap pozwala na szybkie
        //znalezienie pól sąsiadujących z aktualnie badanym polem.

        mapWeights = new int[map.Size, map.Size];
        //Aktualnie analizowany punkt z funkcjami wagowymi
        AStarPoint actualAStarPoint;

        //Lista punktów do przeszukania
        List<AStarPoint> searchAStarPoints = new List<AStarPoint>();
        //Tablica sąsiedztwa osiągniętych pól mapy z wagami funkcji dystansu
        AStarPoint[,] aStarPointsMap = new AStarPoint[map.Size, map.Size];
        //Flaga czy ścieżka została znaleziona
        bool pathIsFounded = false;
        //Lista nie odwiedzonych pól sąsiadujących z aktualnym
        List<AStarPoint> unvisitedNeighboursPoints = new List<AStarPoint>();

        //Uzupełnienie tablicy wag położeniem przeszkód
        foreach (Obstacle obstacle in map.Obstacles)
            foreach (Vector2Int obstaclePoint in obstacle.GetPositions())
                mapWeights[obstaclePoint.x, obstaclePoint.y] = -1;

        //Utworzenie punktu startowego poszukiwań
        actualAStarPoint = new AStarPoint(map.StartPoint, 0, 0);
        searchAStarPoints.Add(actualAStarPoint);
        aStarPointsMap[actualAStarPoint.position.x, actualAStarPoint.position.y] = actualAStarPoint;


        while (searchAStarPoints.Count > 0)
        {
            //Sortowanie nieprzeszukanych punktów pod względem wartości funkcji potencjalnego dystansu między startem i końcem
            searchAStarPoints.Sort((a, b) => a.f.CompareTo(b.f));
            //Wybranie punktu o najmniejszej wartosci potencjalnego dystansu i usunięcie z listy
            actualAStarPoint = searchAStarPoints[0];
            searchAStarPoints.RemoveAt(0);
            //Oznaczenie punktu jako odwiedzony
            actualAStarPoint.visited = true;

            unvisitedNeighboursPoints.Clear();
            //Utworzenie listy nieodwiedzonych punktów sąsiadujących z aktualnym polem
            foreach(Vector2Int neighbour in GetNeighbourFields(actualAStarPoint.position))
            {
                //Wybierz pola nie będące przeszkodami
                if (mapWeights[neighbour.x, neighbour.y] != -1)
                {
                    //Jeśli pole nie istnieje to je utwórz. Jeśli istnieje i nie było odwiedzone to dodaj je w celu weryfikacji
                    if (aStarPointsMap[neighbour.x, neighbour.y] != null)
                    { 
                        if (!aStarPointsMap[neighbour.x, neighbour.y].visited)
                            unvisitedNeighboursPoints.Add(aStarPointsMap[neighbour.x, neighbour.y]);
                    }
                    else
                        unvisitedNeighboursPoints.Add(new AStarPoint(neighbour));

                }
            }


            //Analiza każdego z pól sąsiadujących
            foreach (var neighbour in unvisitedNeighboursPoints)
            {
                //Jeśli pole jest polem końcowym to wyznacz trasę i zakończ poszukiwania
                if (neighbour.position.Equals(map.EndPoint))
                {
                    searchAStarPoints.Clear();
                    map.ShortestPath = GetShortestPathForAStar(actualAStarPoint);
                    pathIsFounded = true;
                    break;
                }

                //Jeśli pole nie zostało wcześniej oznaczone na mapie lub aktualnie znaleziono do niego krótszą drogę
                if (aStarPointsMap[neighbour.position.x, neighbour.position.y] == null || (actualAStarPoint.g + 1) < neighbour.g)
                {
                    //Określ drogę wymaganą do osiągnięcia pola
                    neighbour.g = actualAStarPoint.g + 1;
                    //Wyznacz optymistyczny dystans do osiągnięcia końca
                    int functionH = Mathf.Abs(neighbour.position.x - map.EndPoint.x) + Mathf.Abs(neighbour.position.y - map.EndPoint.y);
                    //Zapisz przewidywany dystans od początku do końca trasy
                    neighbour.f = neighbour.g + functionH;
                    //Zapisz punkt z którego można się do niego najszybciej dostać
                    neighbour.parent = actualAStarPoint;
                    searchAStarPoints.Add(neighbour);
                    //Jeśli nie ma go w tablicy to dodaj go do tablicy i do listy przyszłych poszukiwań
                    if (aStarPointsMap[neighbour.position.x, neighbour.position.y] == null)
                    {
                        aStarPointsMap[neighbour.position.x, neighbour.position.y] = neighbour;
                    }
                }
            }
        }

        return pathIsFounded;
    }

    /// <summary>
    /// Create list of points for shortest path from mapWeights
    /// </summary>
    ///<param name="stepLength">Maximum distance to reach start point</param>
    /// <returns>List of shortest path points</returns>
    static List<Vector2Int> GetShortestPathForAStar(AStarPoint endPathPoint)
    {
        List<Vector2Int> path = new List<Vector2Int>();
        while (endPathPoint.parent != null)
        {
            path.Insert(0, endPathPoint.position);
            endPathPoint = endPathPoint.parent;
        }
        return path;
    }

        #endregion
 }
