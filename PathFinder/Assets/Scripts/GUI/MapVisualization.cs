﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Game object for map visualization control
/// </summary>
public class MapVisualization : MonoBehaviour
{
    #region Fields
    /// <summary>
    /// Texture for map visualization in image
    /// </summary>
    Texture2D mapTexture;

    /// <summary>
    /// RawImage for texture with map visualization
    /// </summary>
    RawImage rawImage;
    #endregion

    #region Properties

    Map _mapData;
    /// <summary>
    /// Property with map data for visualization 
    /// </summary>
    /// <value>Property gets/sets the field with map data and actualize events connection</value>
    public Map MapData
    {
        get
        {
            return _mapData;
        }
        set
        {
            if (value != _mapData)
            {
                if (_mapData != null)
                {
                    //Odpinanie eventów z poprzedniej mapy
                    _mapData.OnObstaclesChange.RemoveListener(DrawObstacles);
                    _mapData.OnStartEndChange.RemoveListener(DrawStartEnd);
                    _mapData.OnShortestPathChange.RemoveListener(DrawPath);
                }
                _mapData = value;
                if (_mapData != null)
                {
                    //Podpinanie eventów z nowej mapy
                    _mapData.OnObstaclesChange.AddListener(DrawObstacles);
                    _mapData.OnStartEndChange.AddListener(DrawStartEnd);
                    _mapData.OnShortestPathChange.AddListener(DrawPath);
                    InitializeTexture(_mapData.Size);
                    DrawMap();
                }
            }
        }
    }

    #endregion
    
    void Start()
    {
        rawImage = GetComponent<RawImage>();    
    }

    /// <summary>
    /// Create new map
    /// </summary>
    /// <param name="sizeMap">Size of new map</param>
    /// <param name="numObstacles">Number of obstacles for new map</param>
    public void CreateMap(int sizeMap,int numObstacles)
    {
        MapData = new Map(sizeMap);
        MapData.GenerateObstacles(numObstacles);
        MapData.GenerateStartEnd();
    }

    /// <summary>
    /// Initialize texture of new map size
    /// </summary>
    /// <param name="sizeMap">Size of new map</param>
    void InitializeTexture(int sizeMap)
    {
        mapTexture = new Texture2D(sizeMap, sizeMap);
        mapTexture.filterMode = FilterMode.Point;
        rawImage.texture = mapTexture;

        for(int i =0;i<sizeMap;i++)
            for (int j = 0; j < sizeMap; j++)
                mapTexture.SetPixel(i, j, Color.white);

        mapTexture.Apply();
    }

    /// <summary>
    /// Generate new obstacles for existing map
    /// </summary>
    /// <param name="numObstacles">Number of obstacles for new map</param>
    public void ChangeObstacles(int numObstacles)
    {
        ClearPoints(MapData.GetAllObstaclesPositions());
        ClearPoints(MapData.ShortestPath);
        MapData.GenerateObstacles(numObstacles);
    }

    /// <summary>
    /// Generate new start/end point for existing map
    /// </summary>
    public void ChangeStartEnd()
    {
        ClearPoints(new List<Vector2Int>() { _mapData.StartPoint, _mapData.EndPoint });
        ClearPoints(MapData.ShortestPath);
        MapData.GenerateStartEnd();
    }

    /// <summary>
    /// Start path finding algorithm
    /// </summary>
    /// <param name="algorithm">Type of selected algorithm</param>
    /// <returns>Return true if path is founded</returns>
    public bool FindPath(PathFinder.Algorithm algorithm)
    {
        ClearPoints(MapData.ShortestPath);
        return PathFinder.FindPath(MapData, algorithm);
    }

    /// <summary>
    /// Draw full map in texture
    /// </summary>
    void DrawMap()
    {
        DrawObstacles(MapData.GetAllObstaclesPositions());
        DrawStartEnd(MapData.StartPoint, MapData.EndPoint);
        DrawPath(MapData.ShortestPath);
    }

    /// <summary>
    /// Draw obstacles in texture
    /// </summary>
    /// <param name="obstaclePositions">List of obstacles positions</param>
    void DrawObstacles(List<Vector2Int> obstaclePositions)
    {
        DrawPoints(obstaclePositions, Color.grey);
    }

    /// <summary>
    /// Draw start/end points in texture
    /// </summary>
    /// <param name="startEndPoints">First element of list is start point, second element is end point</param>
    void DrawStartEnd(List<Vector2Int> startEndPoints)
    {
        if (!startEndPoints[0].Equals(new Vector2Int(-1, -1)) && !startEndPoints[1].Equals(new Vector2Int(-1, -1)))
        {
            DrawPoints(new List<Vector2Int> { startEndPoints[0] }, Color.green);
            DrawPoints(new List<Vector2Int> { startEndPoints[1] }, Color.red);
        }
    }

    /// <summary>
    /// Draw start/end points in texture
    /// </summary>
    /// <param name="startPoint">Start point on map</param>
    /// <param name="endPoint">End point on map</param>
    void DrawStartEnd(Vector2Int startPoint,Vector2Int endPoint)
    {
        if (!startPoint.Equals(new Vector2Int(-1, -1)) && !endPoint.Equals(new Vector2Int(-1, -1)))
        {
            DrawPoints(new List<Vector2Int> { startPoint }, Color.green);
            DrawPoints(new List<Vector2Int> { endPoint }, Color.red);
        }
    }

    /// <summary>
    /// Draw shortest path in texture
    /// </summary>
    ///<param name="shortestPath">Points of shortest path</param>
    void DrawPath(List<Vector2Int> shortestPath)
    {
        DrawPoints(shortestPath, Color.yellow);
    }

    /// <summary>
    /// Clear selected points in texture
    /// </summary>
    ///<param name="clearPositions">Points to clear</param>
    void ClearPoints(List<Vector2Int> clearPositions)
    {
        DrawPoints(clearPositions, Color.white);
    }

    /// <summary>
    /// Draw selected points in texture
    /// </summary>
    ///<param name="positions">Points to draw</param>
    ///<param name="color">Color of drawn points</param>
    void DrawPoints(List<Vector2Int> positions,Color color)
    {
        foreach (Vector2Int position in positions)
        {
            mapTexture.SetPixel(position.x, position.y, color);
        }
        mapTexture.Apply();
    }
}
