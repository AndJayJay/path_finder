﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Linq;

/// <summary>
/// Game object for interface control
/// </summary>
public class Interface : MonoBehaviour
{
    #region Fields
    /// <summary>
    /// InputField for size of new map
    /// </summary>
    public InputField inputMapSize;

    /// <summary>
    /// InputField for number of obstacles for new map
    /// </summary>
    public InputField inputObstaclesNumber;

    /// <summary>
    /// InputField for map name to save
    /// </summary>
    public InputField inputMapName;

    /// <summary>
    /// Dropdown for list of map files
    /// </summary>
    public Dropdown dropMapList;

    /// <summary>
    /// Dropdown for list of avaible algorithms
    /// </summary>
    public Dropdown dropAlgorithms;

    /// <summary>
    /// Text for show notifications
    /// </summary>
    public Text textNotification;

    /// <summary>
    /// Control with visualization of map
    /// </summary>
    public MapVisualization mapVisualization;

    #endregion

    #region Methods

    void Start()
    {
        InitializeControls();
    }

    /// <summary>
    /// Initialize all controls on start
    /// </summary>
    void InitializeControls()
    {
        inputMapSize.characterValidation = InputField.CharacterValidation.Integer;
        inputObstaclesNumber.characterValidation = InputField.CharacterValidation.Integer;
        inputMapName.characterValidation = InputField.CharacterValidation.Alphanumeric;

        dropAlgorithms.AddOptions(Enum.GetNames(typeof(PathFinder.Algorithm)).ToList());
        dropMapList.AddOptions(Serializer.GetFileList("Maps",".mymap"));
    }

    /// <summary>
    /// Generate new map on click
    /// </summary>
    public void ClickGenerateMap()
    {
        int sizeMap = 10;
        int numberObstacle = 0;

        //Weryfikacja danych wejściowych na utworzenie mapy
        if (int.TryParse(inputMapSize.text,out sizeMap) && int.TryParse(inputObstaclesNumber.text, out numberObstacle) && sizeMap >= 10 && numberObstacle >= 0)
        {
            if(numberObstacle < sizeMap*sizeMap-2)
            {
                mapVisualization.CreateMap(sizeMap, numberObstacle);
                PrintNotification("Wygenerowano nową mapę.");
            }
            else
                PrintNotification("Zbyt duża liczba przeszkód");
        }
        else
        {
            PrintNotification("Złe parametry wejściowe. (Minimalny rozmiar mapy to 10)");
        }
    }

    /// <summary>
    /// Generate new obstacles for existing map on click
    /// </summary>
    public void ClickGenerateObstacles()
    {
        //Sprawdzenie czy mapa istnieje
        if (mapVisualization.MapData != null)
        {
            int numberObstacle = 0;
            int sizeMap = mapVisualization.MapData.Size;

            //Weryfikacja danych wejściowych na generowanie przeszkód
            if (int.TryParse(inputObstaclesNumber.text, out numberObstacle) && numberObstacle >= 0)
            {
                if (numberObstacle < sizeMap * sizeMap - 2)
                {
                    mapVisualization.ChangeObstacles(numberObstacle);
                    PrintNotification("Wygenerowano nowe przeszkody.");
                }
                else
                    PrintNotification("Zbyt duża liczba przeszkód");
            }
            else
            {
                PrintNotification("Niepoprawna liczba przeszkód.");
            }

        }
        else
            PrintNotification("Brak mapy.");
    }

    /// <summary>
    /// Generate new start/end points for existing map on click
    /// </summary>
    public void ClickGenerateStartEnd()
    {
        //Sprawdzenie czy mapa istnieje
        if (mapVisualization.MapData != null)
        {
            mapVisualization.ChangeStartEnd();
            PrintNotification("Wygenerowano nowy start i koniec.");
        }
        else
            PrintNotification("Brak mapy.");
    }

    /// <summary>
    /// Save actual map to file
    /// </summary>
    public void ClickSaveMap()
    {
        //Sprawdzenie czy mapa istnieje
        if (mapVisualization.MapData != null)
        {
            string path = Application.streamingAssetsPath + "/Maps/" + inputMapName.text + ".mymap";
            //Zapisanie mapy do pliku
            try
            {
                Serializer.SerializeObject(path, mapVisualization.MapData);
                dropMapList.ClearOptions();
                dropMapList.AddOptions(Serializer.GetFileList("Maps", ".mymap"));
                PrintNotification("Zapisano mapę.");
            }
            catch (Exception e)
            {
                PrintNotification(e.Message);
            }
        }
        else
            PrintNotification("Brak mapy.");
    }

    /// <summary>
    /// Load map from file
    /// </summary>
    public void ClickLoadMap()
    {
        string path = Application.streamingAssetsPath + "/Maps/" + dropMapList.captionText.text;

        //Załadowanie mapy z pliku
        try
        {
            Map tempMap;
            if (Serializer.DeserializeObject(path, out tempMap))
            {
                mapVisualization.MapData = tempMap;
                PrintNotification("Załadowano mapę: "+ dropMapList.captionText.text+". Romiar: "+tempMap.Size+". Przeszkody: "+tempMap.Obstacles.Count);
            }
            else
                PrintNotification("Nie znaleziono mapy");
        }
        catch(Exception e)
        {
            PrintNotification(e.Message);
        }

    }

    /// <summary>
    /// Refresh list of existing map files
    /// </summary>
    public void ClickRefreshMaps()
    {
        dropMapList.ClearOptions();
        dropMapList.AddOptions(Serializer.GetFileList("Maps", ".mymap"));
        PrintNotification("Odświeżono listę map.");
    }

    /// <summary>
    /// Start path finding algorithm on click
    /// </summary>
    public void ClickFindPath()
    {
        //Sprawdzenie czy mapa istnieje
        if (mapVisualization.MapData != null)
        {
            if (mapVisualization.FindPath((PathFinder.Algorithm)dropAlgorithms.value))
                PrintNotification("Wygenerowano ścieżkę. Długość " + mapVisualization.MapData.ShortestPath.Count);
            else
                PrintNotification("Nie znaleziono.");
        }
        else
            PrintNotification("Brak mapy.");
    }

    /// <summary>
    /// Print notification in text control
    /// </summary>
    /// <param name="notification">Text of notification to print</param>
    void PrintNotification(string notification)
    {
        textNotification.text = notification;
    }

    #endregion

}
