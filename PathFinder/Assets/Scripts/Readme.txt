Created by Andrzej Jezierski
Projekt zrealizowany w celu zaprezentowania implementacji i por�wnania algorytm�w znajduj�cych najkr�tsz� drog�.

Obs�uga:
"Generuj now� map�" - tworzy map� z losowo wygenerowanymi przeszkodami(szary) i punktem pocz�tku(zielony) i ko�ca(czerwony). Rozmiar mapy i liczba przeszk�d zale�y od podanych warto�ci.
"Generuj nowe przeszkody" - tworzy nowe przeszkody
"Generuj nowy pocz�tek i koniec" - tworzy nowe punkty pocz�tku i ko�ca
"Zapisz map�" - zapisuje aktualn� map� do pliku o wybranej nazwie
"Za�aduj map�" - �aduje map� z wybranego pliku
"Od�wie� list� map" - od�wie�a list� plik�w z mapami
"Znajd� nakr�tsz� �cie�k�" - uruchamia wybrany algorytm poszukiwania najkr�tszej �cie�ki