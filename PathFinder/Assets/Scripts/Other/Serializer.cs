﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;

/// <summary>
/// Class for serializing maps
/// </summary>
public static class Serializer
{
    /// <summary>
    /// Serialize selected object to file
    /// </summary>
    /// <param name="filePath">Path of file to output serialized object</param>
    /// <param name="serializedObject">Selected object to serialization</param>
    public static void SerializeObject(string filePath,object serializedObject)
    {
        if (File.Exists(filePath))
            File.Delete(filePath);

        //Tworzenie formattera do serializacji
        BinaryFormatter formatter = GenerateFormatter();

        FileStream stream = File.Create(filePath);
        formatter.Serialize(stream, serializedObject);
        stream.Close();
    }

    /// <summary>
    /// Deserialize object from selected file
    /// </summary>
    /// <param name="filePath">Path of file to deserialization</param>
    /// <param name="deserializedObject">Output object of deserialization</param>
    public static bool DeserializeObject<T>(string filePath, out T deserializedObject)
    {
        deserializedObject = default(T);

        if (!File.Exists(filePath))
            return false;

        //Tworzenie formattera do deserializacji
        BinaryFormatter formatter = GenerateFormatter();

        FileStream stream = File.OpenRead(filePath);
        deserializedObject = (T)formatter.Deserialize(stream);
        stream.Close();

        return true;
    }

    /// <summary>
    /// Checking list of existing file in StreamAssets folder
    /// </summary>
    /// <param name="relativeFolderPath">Path inside of StreamAssets folder</param>
    /// <param name="extension">Extension of looked for files</param>
    /// <returns>List of founded files</returns>
    public static List<string> GetFileList(string relativeFolderPath, string extension)
    {
        string path = Application.streamingAssetsPath + "/"+ relativeFolderPath;
        DirectoryInfo d = new DirectoryInfo(path);
        FileInfo[] files = d.GetFiles("*"+ extension);
        List<string> fileNames = new List<string>();
        foreach (FileInfo file in files)
        {
            fileNames.Add(file.Name);
        }

        return fileNames;
    }

    /// <summary>
    /// Create binary formatter for serialization/deserialization
    /// </summary>
    /// <returns>Binary formatter</returns>
    static BinaryFormatter GenerateFormatter()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        SurrogateSelector surrogateSelector = new SurrogateSelector();

        //Dodawanie wsparcia do serializacji obiektów klasy Vector2Int
        Vector2IntSerializationSurrogate vector2IntSS = new Vector2IntSerializationSurrogate();
        surrogateSelector.AddSurrogate(typeof(Vector2Int), new StreamingContext(StreamingContextStates.All), vector2IntSS);
        formatter.SurrogateSelector = surrogateSelector;

        return formatter;
    }
}

/// <summary>
/// Surrogate for serialization of Vector2Int
/// </summary>
public class Vector2IntSerializationSurrogate : ISerializationSurrogate
{
    // Method called to serialize a Vector2Int object
    public void GetObjectData(System.Object obj, SerializationInfo info, StreamingContext context)
    {
        Vector2Int v3 = (Vector2Int)obj;
        info.AddValue("x", v3.x);
        info.AddValue("y", v3.y);
    }

    // Method called to deserialize a Vector2Int object
    public System.Object SetObjectData(System.Object obj, SerializationInfo info,StreamingContext context, ISurrogateSelector selector)
    {
        Vector2Int v3 = (Vector2Int)obj;
        v3.x = (int)info.GetValue("x", typeof(int));
        v3.y = (int)info.GetValue("y", typeof(int));
        obj = v3;
        return obj;
    }
}
