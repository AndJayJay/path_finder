﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Unity event with Vector2Int parameter
/// </summary>
public class ListVector2IntEvent : UnityEvent<List<Vector2Int>>
{
}

/// <summary>
/// Unity event with string parameter
/// </summary>
public class StringEvent : UnityEvent<string>
{
}

/// <summary>
/// Unity event with bool parameter
/// </summary>
public class BoolEvent : UnityEvent<bool>
{
}