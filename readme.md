Projekt zakłada implementację i prezentację działania różnych algorytmów wyszukiwania drogi. 

Aktualnie zaimplementowane algorytmy:
 A*
 BFS (breadth-first search) przeszukiwanie wszerz

Poza użyciem algorytmu, projekt pozwala na wygenerowanie kwadratowej mapy o zadanym rozmiarze (szerokość) oraz wybranej ilości losowo rozmieszczonych przeszkód.
Na mapie generowane są również punkty początku i końca oznaczone zielonym i czerwonym punktem.

Wygenerowaną mapę można zapisać do pliku o wybranej nazwie. Mapy zapisywane są z wykorzystaniem serializacji binarnej.
Zapisane mapy można później załadować bezpośrednio do projektu.